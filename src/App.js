import React, { Component } from 'react';
import './App.css';

import Person from './person/Person'

class App extends Component {
  state = {
    persons: [
      { name: 'ramby', age: 23 },
      { name: 'test11', age: 25 },
      { name: 'teszz', age: 10 }
    ]
  }

  switchNameHandler = (newName) => {
    // console.log("Clicked!");
    this.setState({
      persons: [
        { name: newName, age: 23 },
        { name: 'test11', age: 25 },
        { name: 'teszz', age: 125 }
      ]
    })
  }

  changeNameHandler = (event) => {
    this.setState({
      persons: [
        { name: 'ramby', age: 23 },
        { name: event.target.value, age: 25 },
        { name: 'teszz', age: 125 }
      ]
    })
  }

  render() {
    const style = {
      backgroundColor: 'white',
      font: 'inherit',
      border: '1x solid blue',
      padding: '8px',
      cursor: 'pointer'
    }

    return (
      <div className="App">
        <h1>React Sample Project.</h1>
        <p>This is really working!</p>
        <button style={style} onClick={() => this.switchNameHandler('RjimLuna')}>Switch Name</button>
        <Person name={this.state.persons[0].name} age={this.state.persons[0].age} />
        <Person name={this.state.persons[1].name} age={this.state.persons[1].age} click={this.switchNameHandler.bind(this, 'Ram!')} change={this.changeNameHandler}>My Hobbies: Racing</Person>
        <Person name={this.state.persons[2].name} age={this.state.persons[2].age} />
      </div>
    );
    // return React.createElement('div', {className: 'App'}, React.createElement('h1', null, 'Is this work now ?'));
  }
}

export default App;
